import React from 'react';
import Person from './Person';

function PeopleList(props) {
    return (
        <div>
            {props.people.map(p =>
                <Person key={p.id} name={p.name} />
            )}
        </div>
    )
};

export default PeopleList;