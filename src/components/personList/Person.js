import React from 'react';
import PropTypes from 'prop-types';
import './Person.css';

function Person(props) { return (
    <div className="person text-center rounded">
        <span>{props.name}</span>
    </div>
)}

Person.propTypes = {
    name: PropTypes.string.isRequired
};

export default Person;