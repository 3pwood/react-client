import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from '../view/Home';
import List from './List';

let View = () => (
    <main className='container'>
        <Switch>
            <Route exact path='/' component={Home}  />
            <Route path='/roster' component={List} />
        </Switch>
    </main>
)

export default View