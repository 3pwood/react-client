import React from 'react';
import axios from 'axios';

import Person from '../components/personList/Person';
import PeopleList from '../components/personList/PeopleList';

class List extends React.Component {

    state = {
        people: []
    };

    componentWillMount() {
        axios
        .get('http://192.168.1.21:8000/people')
        .then(res => {
            const newPeople = res.data.map(p => {
                return { id: p.id, name: p.firstname + ' ' + p.lastname };
            });
            this.setState({ people: newPeople })
        })
        .catch(err => console.log(err));
    }
    
    render() {
        return (
            <div>
                <Person name='Someone Useless' />
                <PeopleList people={this.state.people} />
            </div>
        )
    }   
}

export default List;